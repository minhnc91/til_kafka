package example1;

import org.apache.commons.cli.*;

/**
 * Created by Minh NC on 5/23/2017.
 */
public class KafkaProducerDemo {
    public static void main(String[] args) {
        int start = 1;
        int end = 100;

        Options options = new Options();
        options.addOption("s", "start", true, "Start");
        options.addOption("e", "end", true, "End");
//        options.addOption("sync", "sync", false, "sync");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("s")) {
                start = Integer.parseInt(line.getOptionValue("s"));
            }
            if (line.hasOption("e")) {
                end = Integer.parseInt(line.getOptionValue("e"));
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        Producer producerThread = new Producer(KafkaProperties.TOPIC, false, start, end);
        producerThread.start();
    }
}
